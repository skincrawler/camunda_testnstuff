package de.svi.test.xsd_test.plugin.schedule;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin;
import org.springframework.stereotype.Component;

@Component
public class ScheduleJobHandlerPlugin implements ProcessEnginePlugin {

	@Override
	public void preInit(ProcessEngineConfigurationImpl processEngineConfiguration) {
	}

	@Override
	public void postInit(ProcessEngineConfigurationImpl processEngineConfiguration) {
		processEngineConfiguration.getJobHandlers().put(ScheduleJobHandler.TYPE, new ScheduleJobHandler());

	}

	@Override
	public void postProcessEngineBuild(ProcessEngine processEngine) {

	}

}
