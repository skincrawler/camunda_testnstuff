package de.svi.test.xsd_test.event;

import java.util.Map;

import de.svi.bpmn.common.event.AbstractDomainEvent;

public class GetCountryEvent extends AbstractDomainEvent {

	
	private static final String COUNTRY_CODE_VARIABLE_NAME = "countrycode";

	private static final long serialVersionUID = 588590042095431846L;
	private String countryCode;

	public GetCountryEvent(String countryCode) {
		super();
		this.countryCode = countryCode;
	}

	public GetCountryEvent() {
		super();
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public Map<String, Object> getProcessVariables() {
		Map<String, Object> variablesMap = super.getProcessVariables();

		variablesMap.put(COUNTRY_CODE_VARIABLE_NAME, this.countryCode);

		return variablesMap;
	}

}
