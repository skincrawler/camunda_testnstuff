package de.svi.test.xsd_test.listener;

import java.util.Calendar;
import java.util.Date;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.delegate.Expression;
import org.camunda.bpm.engine.impl.context.Context;
import org.camunda.bpm.engine.impl.jobexecutor.TimerDeclarationImpl;
import org.camunda.bpm.engine.impl.jobexecutor.TimerDeclarationType;
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity;
import org.camunda.bpm.engine.impl.persistence.entity.MessageEntity;
import org.camunda.bpm.engine.impl.persistence.entity.TimerEntity;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.stereotype.Component;

import de.svi.test.xsd_test.plugin.schedule.ScheduleJobHandler;

@Component
public class ProcessSchedulerExecutionListener implements ExecutionListener {

	public void notify(DelegateExecution execution) throws Exception {

		// MessageEntity scheduleJob = new MessageEntity()
		Long dueDateLong = getDueDate().getTime();
		DateTimeFormatter dateTimeFormatter = ISODateTimeFormat.dateTime();
		String isoDate = dateTimeFormatter.print(dueDateLong);

		Expression dueDate = Context.getProcessEngineConfiguration().getExpressionManager().createExpression(isoDate);

		TimerDeclarationImpl timerDeclaration = new TimerDeclarationImpl(dueDate, TimerDeclarationType.DATE,
				ScheduleJobHandler.TYPE);

		// MessageEntity scheduleJob = new MessageEntity();
		TimerEntity scheduleJob = timerDeclaration.createTimer((ExecutionEntity) execution);

		// scheduleJob.setActivityId(execution.getCurrentActivityId());
		// scheduleJob.setProcessInstanceId(execution.getProcessInstanceId());
		// scheduleJob.setProcessDefinitionId(execution.getProcessDefinitionId());
		// scheduleJob.setProcessDefinitionKey(execution.getPro);

		// scheduleJob.setJobHandlerConfiguration(new
		// ScheduleJobHandler().newConfiguration("process-start"));

		// Context.getCommandContext().getJobManager().schedule(scheduleJob);

		System.out.println("job:" + scheduleJob.getId());

	}

	private Date getDueDate() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.SECOND, 9999999);
		return c.getTime();

	}

}
