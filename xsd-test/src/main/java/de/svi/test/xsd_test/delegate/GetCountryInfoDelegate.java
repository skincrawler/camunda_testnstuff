package de.svi.test.xsd_test.delegate;

import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import de.svi.test.xsd_test.exception.ServiceFault;
import de.svi.test.xsd_test.exception.ServiceFaultException;
import io.spring.guides.gs_producing_web_service.Currency;

@Service
public class GetCountryInfoDelegate implements JavaDelegate {

	@Value("${de.svi.test.variable.country.code}")
	private String COUNTRY_CODE_VARIABLE_NAME;
	@Value("${de.svi.test.variable.country.capital}")
	private String COUNTRY_CAPITAL_VARIABLE_NAME;
	@Value("${de.svi.test.variable.country.currency}")
	private String COUNTRY_CURRENCY_VARIABLE_NAME;
	@Value("${de.svi.test.variable.country.name}")
	private String COUNTRY_NAME_VARIABLE_NAME;
	@Value("${de.svi.test.variable.country.population}")
	private String COUNTRY_POPULATION_VARIABLE_NAME;

	@Value("${de.svi.test.variable.error.code}")
	private String ERROR_CODE_VARIABLE_NAME;
	@Value("${de.svi.test.variable.error.message}")
	private String ERROR_MESSAGE_VARIABLE_NAME;

	@Value("${de.svi.test.variable.process.country.is.eu}")
	private String EU_COUNTRY_VARIABLE_NAME;

	@Value("${de.svi.test.error.code.prefix}")
	private String ERROR_PREFIX;

	@Value("${de.svi.test.error.code.country.notfound}")
	private String ERROR_COUNTRY_NOT_FOUND;

	@Override
	public void execute(DelegateExecution execution) throws Exception {

		String countrycode = execution.getVariable(COUNTRY_CODE_VARIABLE_NAME).toString();

		switch (countrycode) {

		case "GER":
			execution.setVariable(COUNTRY_CAPITAL_VARIABLE_NAME, "Berlin");
			execution.setVariable(COUNTRY_CURRENCY_VARIABLE_NAME, Currency.EUR);
			execution.setVariable(COUNTRY_NAME_VARIABLE_NAME, countrycode);
			execution.setVariable(COUNTRY_POPULATION_VARIABLE_NAME, 82);
			execution.setVariable(EU_COUNTRY_VARIABLE_NAME, true);
			break;
		case "ENG":
			execution.setVariable(COUNTRY_CAPITAL_VARIABLE_NAME, "London");
			execution.setVariable(COUNTRY_CURRENCY_VARIABLE_NAME, Currency.GBP);
			execution.setVariable(COUNTRY_NAME_VARIABLE_NAME, countrycode);
			execution.setVariable(COUNTRY_POPULATION_VARIABLE_NAME, 53);
			break;
		case "POL":
			execution.setVariable(COUNTRY_CAPITAL_VARIABLE_NAME, "Warsaw");
			execution.setVariable(COUNTRY_CURRENCY_VARIABLE_NAME, Currency.PLN);
			execution.setVariable(COUNTRY_NAME_VARIABLE_NAME, countrycode);
			execution.setVariable(COUNTRY_POPULATION_VARIABLE_NAME, 38);
			execution.setVariable(EU_COUNTRY_VARIABLE_NAME, false);
			break;
		case "BER":
			throw new ServiceFaultException(countrycode + " is not a Country.",
					new ServiceFault("Fault_NotACountry", countrycode + " is not a Country."));
		default:

			throw new BpmnError(ERROR_PREFIX + ERROR_COUNTRY_NOT_FOUND, "No Country with CountryCode = " + countrycode);

		}

	}

}
