package de.svi.test.xsd_test.controller;

import java.util.List;
import java.util.Map.Entry;

import javax.xml.bind.JAXBElement;

import org.camunda.bpm.engine.ExternalTaskService;
import org.camunda.bpm.engine.externaltask.LockedExternalTask;
import org.camunda.bpm.engine.variable.VariableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import de.svi.xsd.errorhandler.ErrorTaskType;
import de.svi.xsd.errorhandler.ErrorTaskType.Variable;
import de.svi.xsd.errorhandler.ObjectFactory;
import de.svi.xsd.errorhandler.RequestType;
import de.svi.xsd.errorhandler.ResponseType;

@Endpoint
public class ErrorHandler {

	private static final String ERROR_HANDLER_WSDL_NS = "http://svi.de/xsd/ErrorHandler/";
	private static final String GET_TASK_LIST_MSG_NAME = "getTaskListRequest";

	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);

	@Value("${de.svi.test.error.code.worker.id}")
	private String ERROR_WORKER_ID;
	@Value("${de.svi.test.error.code.worker.topic}")
	private String ERROR_WORKER_TOPIC;
	@Value("${de.svi.test.variable.error.code}")
	private String ERROR_CODE_VARIABLE;
	@Value("${de.svi.test.variable.error.message}")
	private String ERROR_MESSAGE_VARIABLE;

	private final static String START_EVENT_VARIABLE = "startEvent";
	private final static long DEFAULT_TTL = 60L * 1000L;
	private final static int DEFAULT_FETCH_MSG = 10;

	ObjectFactory objFactory = new ObjectFactory();

	@Autowired
	ExternalTaskService externalTaskService;

	@PayloadRoot(namespace = ERROR_HANDLER_WSDL_NS, localPart = GET_TASK_LIST_MSG_NAME)
	@ResponsePayload
	private JAXBElement<ResponseType> doTasklist(@RequestPayload RequestType request) {
		Long ttl;
		try {
			ttl = request.getTimeToLife().longValue();
		} catch (Exception e) {
			ttl = DEFAULT_TTL;
		}
		int fetch_msg;
		try {
			fetch_msg = request.getFetchMessageAmount().intValue();
		} catch (Exception e) {
			fetch_msg = DEFAULT_FETCH_MSG;
		}

		List<LockedExternalTask> tasks = externalTaskService.fetchAndLock(fetch_msg, ERROR_WORKER_ID)
				.topic(ERROR_WORKER_TOPIC, ttl).execute();

		ResponseType response = new ResponseType();

		for (LockedExternalTask task : tasks) {
			VariableMap variables = task.getVariables();

			String errorCode = variables.getValue(ERROR_CODE_VARIABLE, String.class);
			String errorMessage = variables.getValue(ERROR_MESSAGE_VARIABLE, String.class);

			LOGGER.debug("Locked for {} Task {} => '{}'", ttl, errorCode, errorMessage);

			ErrorTaskType errorTask = new ErrorTaskType();
			errorTask.setErrorCode(errorCode);
			errorTask.setErrorMessage(errorMessage);

			Variable errorTaskVariable;

			for (Entry<String, Object> variable : variables.entrySet()) {
				if (!variable.getKey().equals(ERROR_CODE_VARIABLE) && !variable.getKey().equals(ERROR_MESSAGE_VARIABLE)
						&& !variable.getKey().equals(START_EVENT_VARIABLE)) {
					errorTaskVariable = new Variable();
					errorTaskVariable.setKey(variable.getKey());
					errorTaskVariable.setValue(variable.getValue());
					errorTask.getVariable().add(errorTaskVariable);
				}
			}

			response.getErrorTask().add(errorTask);

		}

		return objFactory.createGetTaskListResponse(response);

	}

}
