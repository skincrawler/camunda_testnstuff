package de.svi.test.xsd_test.plugin.schedule;

import org.camunda.bpm.engine.impl.interceptor.CommandContext;
import org.camunda.bpm.engine.impl.jobexecutor.AsyncContinuationJobHandler;
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity;

public class ScheduleJobHandler extends AsyncContinuationJobHandler {

	public final static String TYPE = "schedule-process-instance";

	@Override
	public String getType() {
		return TYPE;
	}

	public ScheduleJobHandler() {
		super();
	}

	@Override
	public void execute(AsyncContinuationConfiguration configuration, ExecutionEntity execution,
			CommandContext commandContext, String tenantId) {
		super.execute(configuration, execution, commandContext, tenantId);
		System.out.println("execute ScheduleJobHandler");
		System.out.println("configuration=" + configuration);
		System.out.println("execution=" + execution);
		System.out.println("command=" + commandContext);
		System.out.println("tenantId=" + tenantId);

	}

}
