package de.svi.test.xsd_test.configuration;

import java.util.Properties;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.soap.server.endpoint.SoapFaultDefinition;
import org.springframework.ws.soap.server.endpoint.SoapFaultMappingExceptionResolver;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.SimpleWsdl11Definition;
import org.springframework.ws.wsdl.wsdl11.Wsdl11Definition;

import de.svi.test.xsd_test.exception.DetailSoapFaultDefinitionExceptionResolver;
import de.svi.test.xsd_test.exception.ServiceFaultException;

@EnableWs
@Configuration
public class SoapTestConfiguration {

	private static final String WSDL_RESOURCE_PATH = "wsdl/";
	private static final String COUNTRIES_WSDL_NAME = "countries.wsdl";
	private static final String ERROR_HANDLER_WSDL_NAME = "ErrorHandler.wsdl";
	private static final String LOCATION_URI = "/test";

	@Bean
	public SoapFaultMappingExceptionResolver exceptionResolver() {
		SoapFaultMappingExceptionResolver exceptionResolver = new DetailSoapFaultDefinitionExceptionResolver();

		SoapFaultDefinition soapFaultDefinition = new SoapFaultDefinition();
		soapFaultDefinition.setFaultCode(SoapFaultDefinition.SERVER);
		exceptionResolver.setDefaultFault(soapFaultDefinition);

		Properties errorMappings = new Properties();
		errorMappings.setProperty(Exception.class.getName(), SoapFaultDefinition.SERVER.toString());
		errorMappings.setProperty(ServiceFaultException.class.getName(), SoapFaultDefinition.SERVER.toString());
		exceptionResolver.setExceptionMappings(errorMappings);
		exceptionResolver.setOrder(1);

		return exceptionResolver;
	}

	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();

		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);

		return new ServletRegistrationBean(servlet, LOCATION_URI + "/*");

	}

	@Bean(name = "countries")
	public Wsdl11Definition defaultWsdl11Defintion() {
		SimpleWsdl11Definition wsdl11Definition = new SimpleWsdl11Definition();
		wsdl11Definition.setWsdl(new ClassPathResource(WSDL_RESOURCE_PATH + COUNTRIES_WSDL_NAME));
		return wsdl11Definition;
	}

	@Bean(name = "errors")
	public Wsdl11Definition errorHandlerWsdl11Definition() {
		SimpleWsdl11Definition wsdl11Definition = new SimpleWsdl11Definition();
		wsdl11Definition.setWsdl(new ClassPathResource(WSDL_RESOURCE_PATH + ERROR_HANDLER_WSDL_NAME));
		return wsdl11Definition;
	}
}