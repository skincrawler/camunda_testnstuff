package de.svi.test.xsd_test.controller;

import java.util.UUID;

import javax.xml.bind.JAXBElement;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import de.svi.bpmn.common.service.BpmnInstanceManagerService;
import de.svi.test.xsd_test.event.GetCountryEvent;
import io.spring.guides.gs_producing_web_service.AsynchResponseType;
import io.spring.guides.gs_producing_web_service.Country;
import io.spring.guides.gs_producing_web_service.Currency;
import io.spring.guides.gs_producing_web_service.ObjectFactory;
import io.spring.guides.gs_producing_web_service.RequestType;
import io.spring.guides.gs_producing_web_service.SynchResponseType;

@Endpoint
public class TestMessageController {

	private static final String GET_COUNTRY_WSDL_NS = "http://spring.io/guides/gs-producing-web-service";
	private static final String GET_COUNTRY_MSG_NAME = "getCountryRequest";
	private static final String GET_COUNTRY_ASYNCH_MSG_NAME = "getCountryAsynchRequest";

	private ObjectFactory objFactory = new ObjectFactory();

	@Value("${de.svi.test.bpmn.model.synch.id}")
	private String BPMN_MODEL_ID_SYNCH;
	@Value("${de.svi.test.bpmn.model.asynch.id}")
	private String BPMN_MODEL_ID_ASYNCH;

	@Value("${de.svi.test.variable.country.capital}")
	private String COUNTRY_CAPITAL_VARIABLE_NAME;
	@Value("${de.svi.test.variable.country.currency}")
	private String COUNTRY_CURRENCY_VARIABLE_NAME;
	@Value("${de.svi.test.variable.country.name}")
	private String COUNTRY_NAME_VARIABLE_NAME;
	@Value("${de.svi.test.variable.country.population}")
	private String COUNTRY_POPULATION_VARIABLE_NAME;

	@Autowired
	private RuntimeService runtimeService;

	@PayloadRoot(namespace = GET_COUNTRY_WSDL_NS, localPart = GET_COUNTRY_MSG_NAME)
	@ResponsePayload
	public JAXBElement<SynchResponseType> startProcessService(@RequestPayload JAXBElement<RequestType> request) {

		GetCountryEvent event = new GetCountryEvent(request.getValue().getName());
		event.setBusinessKey(UUID.randomUUID().toString());

		ProcessInstance processInstance = BpmnInstanceManagerService.processEvent(runtimeService, event,
				BPMN_MODEL_ID_SYNCH);

		SynchResponseType response = new SynchResponseType();
		Country country = new Country();

		country.setCapital(runtimeService
				.getVariable(processInstance.getProcessInstanceId(), COUNTRY_CAPITAL_VARIABLE_NAME).toString());
		country.setCurrency((Currency) runtimeService.getVariable(processInstance.getProcessInstanceId(),
				COUNTRY_CURRENCY_VARIABLE_NAME));
		country.setName(runtimeService.getVariable(processInstance.getProcessInstanceId(), COUNTRY_NAME_VARIABLE_NAME)
				.toString());
		country.setPopulation(Integer.parseInt(runtimeService
				.getVariable(processInstance.getProcessInstanceId(), COUNTRY_POPULATION_VARIABLE_NAME).toString()));

		response.setCountry(country);

		return objFactory.createGetCountryResponse(response);

	}

	@PayloadRoot(namespace = GET_COUNTRY_WSDL_NS, localPart = GET_COUNTRY_ASYNCH_MSG_NAME)
	@ResponsePayload
	public JAXBElement<AsynchResponseType> startProcessServiceAsynch(@RequestPayload JAXBElement<RequestType> request) {

		GetCountryEvent event = new GetCountryEvent(request.getValue().getName());
		event.setBusinessKey(UUID.randomUUID().toString());

		ProcessInstance processInstance = BpmnInstanceManagerService.processEvent(runtimeService, event,
				BPMN_MODEL_ID_ASYNCH);
		AsynchResponseType response = new AsynchResponseType();

		response.setBusinessKey(processInstance.getBusinessKey());
		response.setProcessInstanceId(processInstance.getProcessInstanceId());

		return objFactory.createGetCountryAsynchResponse(response);

	}

}
