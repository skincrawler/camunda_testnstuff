package de.svi.test.xsd_test.exception;

public class ServiceFaultException extends RuntimeException {

	private static final long serialVersionUID = -8125350509197971679L;
	
	 private ServiceFault serviceFault;

	    public ServiceFaultException(String message, ServiceFault serviceFault) {
	        super(message);
	        this.serviceFault = serviceFault;
	    }

	    public ServiceFaultException(String message, Throwable e, ServiceFault serviceFault) {
	        super(message, e);
	        this.serviceFault = serviceFault;
	    }

	    public ServiceFault getServiceFault() {
	        return serviceFault;
	    }

	    public void setServiceFault(ServiceFault serviceFault) {
	        this.serviceFault = serviceFault;
	    }

}
