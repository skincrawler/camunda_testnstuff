package de.svi.test.xsd_test;

import static org.assertj.core.api.Assertions.*;

import org.camunda.bpm.engine.ProcessEngines;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.svi.test.xsd_test.controller.TestMessageController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProcessApplicationApplicationTests {

	@Autowired
	private TestMessageController controller;

	@Before
	public void setUp() {
		ProcessEngines.init();
	}

	@After
	public void tearDown() {
		ProcessEngines.destroy();
	}

	@Test
	public void contextLoads() {

		assertThat(controller).isNotNull();
	}
}
